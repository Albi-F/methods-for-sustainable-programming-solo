## How to execute the program

1. Install Docker Desktop (https://www.docker.com/products/docker-desktop)

2. Install the VS Code extension Remote - Containers (https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

3. Click on the arrows facing each other in the lower left corner on VS Code

4. In the menu that opens up, click on the *Open in Container* entry.

5. Execute ``main.py``

## Coverage

- Run ``make coverage``

## Documentation

- Documentation is automatically built by the CI (https://albi-f.gitlab.io/methods-for-sustainable-programming-solo/)