"""Unit testing for the game class."""


import unittest
from unittest.mock import patch
from unittest import mock
from src.pig.game import Game


class TestGameClass(unittest.TestCase):
    """Test the class."""

    def test_class_variables(self):
        """Test the class variables of the Game class."""
        self.assertEqual(Game.die_faces, 6)
        self.assertEqual(Game.points_for_victory, 100)

    def test_init(self):
        """Test the values of variables in the beginning."""
        newgame = Game("player1", "player2")
        self.assertEqual(len(newgame.players), 2)

    def test_change_player_name(self):
        """Test the function used to change the player's name."""
        newgame = Game("player1", "player2")
        self.assertEqual(newgame.current_player.name, "player1")
        newgame.change_player_name("playerX")
        self.assertEqual(newgame.current_player.name, "playerX")

    def test_next_player_turn(self):
        """Test update of current player pointer."""
        game_test = Game("Albi", "Filo")
        self.assertEqual(game_test.current_player.name, "Albi")
        game_test.next_player_turn()
        self.assertEqual(game_test.current_player.name, "Filo")

    @patch("src.pig.game.Player")
    def test_end_player_turn(self, mock_player):
        """Test action to do when a player's turn ends."""
        newgame = Game("player1", "player2")
        player1_instance = newgame.current_player
        player1_instance.total_points = 100
        returned_value: bool = newgame.end_player_turn()
        player1_instance.end_turn.assert_called_once()
        self.assertEqual(returned_value, True)

        player1_instance.total_points = 4
        returned_value = newgame.end_player_turn()
        self.assertEqual(returned_value, False)

        player1_instance.name = "Cheater"
        player1_instance.total_points = 25
        returned_value = newgame.end_player_turn()
        self.assertEqual(returned_value, True)

    def test_player_points(self) -> None:
        """Test the player_points method."""
        newgame = Game("player1", "player2")

        with mock.patch('src.pig.player.Player.temporary_points',
                        new_callable=mock.PropertyMock) as mock_temporary_points:

            mock_temporary_points.return_value = 10
            self.assertEqual(newgame.player_points()[0], 10)

        with mock.patch('src.pig.player.Player.total_points',
                        new_callable=mock.PropertyMock) as mock_total_points:

            mock_total_points.return_value = 12
            self.assertEqual(newgame.player_points()[1], 12)


if __name__ == '__main__':
    unittest.main()
