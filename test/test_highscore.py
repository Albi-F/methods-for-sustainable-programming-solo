"""Unit testing for the highscore class."""

from typing import Dict, List, Union
import unittest
from src.pig import highscore


class TestHighscoreClass(unittest.TestCase):
    """Test the highscore class."""

    def setUp(self):
        """Execute before each method in this class."""
        self.player_score_dictionary_template: Dict[str, Union[str, int]] = {
            "Name": "",
            "ID": 0,
            "Turns played": 0,
            "Best turn score": 0
        }

        self.best_player_score_entry: Dict[str, Union[str, int]] = dict(self.player_score_dictionary_template)
        self.best_player_score_entry["Name"] = "Best player"
        self.best_player_score_entry["ID"] = 1
        self.best_player_score_entry["Turns played"] = 3
        self.best_player_score_entry["Best turn score"] = 30

        self.second_best_player_score_entry: Dict[str, Union[str, int]] = dict(self.player_score_dictionary_template)
        self.second_best_player_score_entry["Name"] = "Second best player"
        self.second_best_player_score_entry["ID"] = 4
        self.second_best_player_score_entry["Turns played"] = 6
        self.second_best_player_score_entry["Best turn score"] = 22

        self.third_best_player_score_entry: Dict[str, Union[str, int]] = dict(self.player_score_dictionary_template)
        self.third_best_player_score_entry["Name"] = "Third best player"
        self.third_best_player_score_entry["ID"] = 3
        self.third_best_player_score_entry["Turns played"] = 5
        self.third_best_player_score_entry["Best turn score"] = 10

        self.worst_player_score_entry: Dict[str, Union[str, int]] = dict(self.player_score_dictionary_template)
        self.worst_player_score_entry["Name"] = "Worst player"
        self.worst_player_score_entry["ID"] = 2
        self.worst_player_score_entry["Turns played"] = 5
        self.worst_player_score_entry["Best turn score"] = 5

        self.highscore_manager: highscore.Highscore = highscore.Highscore()

    def add_high_scores(self):
        """Add records to the highscore class."""
        self.highscore_manager.add_name_score(
            self.best_player_score_entry["Name"],
            self.best_player_score_entry["ID"],
            [[6, 6, 6, 6, 6], [1], [2, 3], []])

        self.highscore_manager.add_name_score(
            self.worst_player_score_entry["Name"],
            self.worst_player_score_entry["ID"],
            [[5], [4, 1], [2], [5], [1], []])

        self.highscore_manager.add_name_score(
            self.third_best_player_score_entry["Name"],
            self.third_best_player_score_entry["ID"],
            [[5, 1], [4, 4, 2], [6, 1], [6], [6], []])

        self.highscore_manager.add_name_score(
            self.second_best_player_score_entry["Name"],
            self.second_best_player_score_entry["ID"],
            [[1], [1], [2, 5, 6, 5, 4], [5, 5, 5, 5], [4], [3], []])

    def test_add_name_score(self):
        """Test the add name score method."""
        self.add_high_scores()
        players_score_list: List[self.player_score_dictionary_template] = []
        players_score_list.append(self.best_player_score_entry)
        players_score_list.append(self.second_best_player_score_entry)
        players_score_list.append(self.third_best_player_score_entry)
        players_score_list.append(self.worst_player_score_entry)

        self.assertEqual(players_score_list, self.highscore_manager.high_scores)

    def test_print_high_scores(self):
        """Test the print high scores method."""
        expected_in_string: str = "no score"
        returned_string: str = self.highscore_manager.print_high_scores()
        self.assertTrue(expected_in_string.lower() in returned_string.lower(), returned_string)

        self.add_high_scores()
        returned_string = self.highscore_manager.print_high_scores()
        self.assertTrue(all([key in returned_string for key in self.player_score_dictionary_template.keys()]))


if __name__ == '__main__':
    unittest.main()
