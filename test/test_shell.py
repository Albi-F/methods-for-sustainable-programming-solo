"""Unit test for the shell class."""

import io
import unittest
import sys
from src.pig.shell import Shell
from unittest import mock
from unittest.mock import patch


class TestShellClass(unittest.TestCase):
    """Test methods."""

    def test_default(self):
        """Test the default method."""
        # Prepare to capture the output
        captured_output = io.StringIO()
        sys.stdout = captured_output

        # Call the method(s) printing to stdout (and capture the output)
        shell1 = Shell()
        shell1.default("")

        # Reset the capture
        sys.stdout = sys.__stdout__

        # get the captured output
        output = captured_output.getvalue()

        self.assertTrue("not valid" in output)

    @patch("src.pig.shell.Shell.default")
    def test_emptyline(self, mock_default):
        """Test emptyline method."""
        # mock_default.return_value = "ciao"
        # print(Shell.default("", ""))
        shell1 = Shell()
        shell1.emptyline()
        # test that Shell.emptyline called Shell.default once
        mock_default.assert_called_once()

    def test_postcmd(self):
        """Test postcmd function."""
        player1_name = "Albi"
        player2_name = "Filo"
        shell1 = Shell()
        shell1.postcmd("", "")
        self.assertTrue("Pig" in shell1.prompt)
        shell1.do_newgame(player1_name + " " + player2_name)
        shell1.postcmd("", "")
        self.assertTrue(player1_name in shell1.prompt)

    @patch("src.pig.shell.Shell.no_game_started")
    def test_changename(self, mock_no_game_started):
        """Test the changename function."""
        shell1 = Shell()
        shell1.do_changename("Albi")
        mock_no_game_started.assert_called_once()

        shell1.do_newgame("Albi")
        captured_output = io.StringIO()
        sys.stdout = captured_output
        shell1.do_changename("")
        sys.stdout = sys.__stdout__
        output = captured_output.getvalue()
        self.assertTrue("Name not changed" in output)

        shell1.do_changename("Ale")
        self.assertEqual(shell1.current_game.current_player.name, "Ale")

    def test_help_changename(self):
        """Test the help_* methods."""
        # Prepare to capture the output
        captured_output = io.StringIO()
        sys.stdout = captured_output

        # Call the method(s) printing to stdout (and capture the output)
        shell1 = Shell()

        # Test help_changename
        shell1.help_changename()
        output = captured_output.getvalue()
        self.assertTrue(len(output) > 1)

        # Test help_newgame
        shell1.help_newgame()
        output = captured_output.getvalue()
        self.assertTrue(len(output) > 1)

        # Reset the capture
        sys.stdout = sys.__stdout__

    def test_do_cheat(self):
        """Test the do_cheat method."""
        # Prepare to capture the output
        captured_output = io.StringIO()
        sys.stdout = captured_output

        shell1 = Shell()
        shell1.do_newgame("Albi Filo")
        shell1.do_cheat("")

        # Reset the capture
        sys.stdout = sys.__stdout__

        # get the captured output
        output = captured_output.getvalue()
        self.assertTrue(len(output) > 1)

        shell1.do_quit("")
        shell1.do_cheat("")
        self.assertEqual(shell1.current_game.current_player.name, "Cheater")

    def test_do_endturn(self):
        """Test the do_endturn method."""
        mock_shell = mock.create_autospec(Shell)
        mock_shell.do_endturn = lambda x: Shell.do_endturn(mock_shell, x)

        mock_shell.current_game = ""
        mock_shell.do_endturn("")
        mock_shell.ending_turn.assert_called_once()

        mock_shell.current_game = None
        mock_shell.do_endturn("")
        mock_shell.no_game_started.assert_called_once()

    def test_do_exit(self):
        """Test the do_exit method."""
        shell1 = Shell()
        with mock.patch('builtins.input', return_value="y"):
            self.assertTrue(shell1.do_exit(""))

    @patch("src.pig.shell.Highscore.print_high_scores")
    def test_do_highscore(self, mock_print_high_scores):
        """Test the do_highscore function."""
        shell1 = Shell()
        shell1.do_highscore("")
        mock_print_high_scores.assert_called_once()

    def test_do_points(self):
        """Test do_points method."""
        mock_shell = mock.create_autospec(Shell)
        mock_shell.do_points = lambda x: Shell.do_points(mock_shell, x)

        mock_shell.current_game = None
        mock_shell.do_points("")
        mock_shell.no_game_started.assert_called_once()
        mock_shell.print_points.assert_not_called()

        mock_shell.current_game = ""
        mock_shell.do_points("")
        mock_shell.print_points.assert_called_once()
