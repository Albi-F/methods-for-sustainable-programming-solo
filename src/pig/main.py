"""This module is used to start the main cmd loop."""

import src.pig.shell as shell   # pragma: no cover


if __name__ == '__main__':      # pragma: no cover
    shell.Shell().cmdloop()
