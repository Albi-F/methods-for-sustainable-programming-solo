"""This module is used to represent the players."""
from typing import List
import itertools


class Player():
    """Data of the player."""

    id_iter = itertools.count(start=1)

    def __init__(self, name: str):
        """Set the initial points of the player."""
        self.id = next(self.id_iter)
        self.dice_rolled: List[List[int]] = [[]]
        self.name: str = name

    def save_roll(self, points: int) -> None:
        """Add last earned points to temporary points."""
        self.dice_rolled[-1].append(points)

    def end_turn(self) -> None:
        """End current turn."""
        self.dice_rolled.append([])

    @property
    def temporary_points(self) -> int:
        """Get temporary points."""
        return self.turn_points(self.dice_rolled[-1])

    @property
    def total_points(self) -> int:
        """Get total points."""
        total_points: int = 0
        for turn in range(len(self.dice_rolled) - 1):
            total_points += self.turn_points(self.dice_rolled[turn])
        return total_points

    def turn_points(self, turn: List[int]) -> int:
        """Return the points of this turn."""
        if 1 in turn:
            return 0
        return sum(turn)

    def __str__(self) -> str:
        """Return a string defining the object."""
        return "Player name: {}, ID: #{}".format(self.name, self.id)
